#include <iostream>
#include "BST.h"


using namespace std;

int main() {
    std::vector<double> vec;
    vec.push_back(1);
    vec.push_back(8);
    vec.push_back(-5);
    vec.push_back(3);
    vec.push_back(-10);
    BST Tree(vec);
    std::cout << Tree << std::endl;

    std::cout << Tree.find(3) << std::endl;
    return 0;
}