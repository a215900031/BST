//
// Created by zhu on 2017/12/9.
//

#ifndef BST_BST_H
#define BST_BST_H

#include <iostream>
#include <vector>
#include <string>

struct Node {
    Node(double data);

    double data;
    Node *lSon;
    Node *rSon;
};


class BST {
public:
    BST();

    explicit BST(std::istream is);

    explicit BST(std::vector<double> &vec);

    BST &insert(double data);

    bool find(double data);

    BST &delElem(double data);

    BST &doFunction(void (*fun)(double));

    friend std::istream &operator>>(std::istream &is, BST &Tree);

    friend std::ostream &operator<<(std::ostream &os, const BST &Tree);


private:
    Node _root;

    void do_insert(double data, Node *father);

    void do_prt(std::ostream &os, const Node *father) const;

    Node * do_find(double data, Node *father);
};


#endif //BST_BST_H
