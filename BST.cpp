//
// Created by zhu on 2017/12/9.
//

#include "BST.h"


Node::Node(double data) {
    lSon = nullptr;
    rSon = nullptr;
    this->data = data;
}

BST::BST() : _root(0) {

}

BST::BST(std::istream is) : _root(0) {

}

BST::BST(std::vector<double> &vec) : _root(0) {
    for (auto &&item : vec) {
        insert(item);
    }
}

BST &BST::insert(double data) {
    do_insert(data, &_root);
    return *this;
}

void BST::do_insert(double data, Node *father) {
    if (father->lSon == nullptr && father->data > data) {
        father->lSon = new Node(data);
        return;
    }
    if (father->rSon == nullptr && father->data < data) {
        father->rSon = new Node(data);
        return;
    }
    if (father->data > data) {
        do_insert(data, father->lSon);
    } else {
        do_insert(data, father->rSon);
    }
}

bool BST::find(double data) {
    bool flag;
    flag = (do_find(data, &_root) != nullptr);
    return flag;
}


Node *BST::do_find(double data, Node *father) {
    if (father == nullptr) {
        return nullptr;
    }
    if (father->data == data) {
        return father;
    }
    if (father->data > data) {
        return do_find(data, father->lSon);
    } else {
        return do_find(data, father->rSon);
    }
}

std::ostream &operator<<(std::ostream &os, const BST &Tree) {
    Tree.do_prt(os, &Tree._root);
    return os;
}

void BST::do_prt(std::ostream &os, const Node *father) const {
    if (father == nullptr) {
        return;
    }
    do_prt(os, father->lSon);
    os << father->data << '|';
    do_prt(os, father->rSon);
}

