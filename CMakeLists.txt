cmake_minimum_required(VERSION 3.8)
project(BST)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp BST.cpp BST.h)
add_executable(BST ${SOURCE_FILES})